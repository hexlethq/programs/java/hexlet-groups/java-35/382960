package exercise.connections;


import exercise.TcpConnection;

// BEGIN
public class Disconnected implements Connection{
    private TcpConnection tcpConnection;

    public Disconnected(TcpConnection tcpConnection) {
        this.tcpConnection = tcpConnection;
    }

    @Override
    public String getCurrentState() {
        return "disconnected";
    }

    @Override
    public void connect() {
        TcpConnection newTcpConnection = this.tcpConnection;
        tcpConnection.setCurrentState(new Connected(newTcpConnection));
    }

    @Override
    public void disconnect() {
        System.out.println("Error! Connection is already disconnected");
    }

    @Override
    public void write(String data) {
        System.out.println("Error! Try to connect before writing");
    }
}
// END
