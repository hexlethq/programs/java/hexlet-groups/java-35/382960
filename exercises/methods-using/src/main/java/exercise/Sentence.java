package exercise;

import java.util.Locale;

class Sentence {
    public static void printSentence(String sentence) {
        // BEGIN
        String sentenceEnd = sentence.substring(sentence.length()-1);
        if(sentenceEnd.startsWith("!")){
            System.out.println(sentence.toUpperCase());
        }
        else {
            System.out.println(sentence.toLowerCase());
        }
        // END
    }
}
