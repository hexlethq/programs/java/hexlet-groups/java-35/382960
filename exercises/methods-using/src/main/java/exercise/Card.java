package exercise;

class Card {
    public static void printHiddenCard(String cardNumber, int starsCount) {
        // BEGIN
        System.out.println(String.format("%" + (starsCount + 4) + "s", cardNumber.substring(cardNumber.length() - 4)).replace(" ", "*"));
        // END
    }
}
