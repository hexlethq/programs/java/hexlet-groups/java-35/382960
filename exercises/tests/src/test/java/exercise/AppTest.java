package exercise;

import static org.assertj.core.api.Assertions.assertThat;
import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;
import org.junit.jupiter.api.Test;

class AppTest {

    @Test
    void testTake() {
        // BEGIN
        //основная функциональность
        List<Integer> numbers1 = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5));
        int count1 = 2;
        List<Integer> expected1 = new ArrayList<>(Arrays.asList(1, 2));
        List<Integer> actual1 = App.take(numbers1, count1);
        assertThat(actual1).isEqualTo(expected1);

        List<Integer> numbers2 = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 10, 12));
        int count2 = 6;
        List<Integer> expected2 = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 10));
        List<Integer> actual2 = App.take(numbers2, count2);
        assertThat(actual2).isEqualTo(expected2);

        //пограничный случай, если length == 0
        List<Integer> numbers3 = new ArrayList<>();
        int count3 = 2;
        List<Integer> expected3 = new ArrayList<>();
        List<Integer> actual3 = App.take(numbers3, count3);
        assertThat(actual3).isEqualTo(expected3);

        //пограничный случай, если count > length
        List<Integer> numbers4 = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 10));
        int count4 = 12;
        List<Integer> expected4 = List.of(1, 2, 3, 4, 5, 10);
        List<Integer> actual4 = App.take(numbers4, count4);
        assertThat(actual4).isEqualTo(expected4);
//         END
    }
}
