package exercise;

class App {
    // BEGIN
    public static String getAbbreviation(String phrase) {
        String delimiter = " ";
        String[] subStr = phrase.split(delimiter);
        String abbreviation = "";

        for (int i = 0; i < subStr.length; i++) {
            char symbol;
            if (!subStr[i].equals("")) {
                symbol = subStr[i].charAt(0);
                abbreviation = abbreviation + Character.toUpperCase(symbol);
            }
        }
        System.out.println(abbreviation);
        return abbreviation;
    }
    // END
}
