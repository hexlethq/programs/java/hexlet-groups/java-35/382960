package exercise;

class App {
    // BEGIN
    public static int[] sort(int[] numbers) {
        boolean isSorted = true;
        if (numbers.length != 0) {
            while (isSorted) {
                isSorted = false;
                for (int i = 0; i < numbers.length - 1; i++) {
                    if (numbers[i] > numbers[i + 1]) {
                        numbers[i] += numbers[i + 1];
                        numbers[i + 1] = numbers[i] - numbers[i + 1];
                        numbers[i] = numbers[i] - numbers[i + 1];
                        isSorted = true;
                    }
                }
            }
        }
        return numbers;
    }
    // END [10, 1, 3]
//    public static int[] sort(int[] numbers) {
//        if (numbers.length != 0) {
//            for (int i = 0; i < numbers.length; i++) {
//                int pos = i;
//                int min = numbers[i];
//                for (int j = i + 1; j < numbers.length; j++) {
//                    if (numbers[j] < min) {
//                        pos = j;
//                        min = numbers[j];
//                    }
//                }
//                numbers[pos] = numbers[i];
//                numbers[i] = min;
//            }
//        }
//        return numbers;
//    }
}
