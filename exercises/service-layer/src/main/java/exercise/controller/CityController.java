package exercise.controller;
import exercise.model.City;
import exercise.repository.CityRepository;
import exercise.service.WeatherService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Map;
import java.util.List;
import java.util.stream.Collectors;


@RestController
public class CityController {

    @Autowired
    private CityRepository cityRepository;

    @Autowired
    private WeatherService weatherService;

    // BEGIN
    @GetMapping(path = "/cities/{id}")
    public Map getCityInfo(@PathVariable(value = "id") long id) {
        return weatherService.getWeather(id);
    }

    @GetMapping(path = "/search")
    public List<Map> getListCities(@RequestParam(required = false) String name) {
        List<City> cities = name == null
                ? cityRepository.findAllByOrderByName() :
                cityRepository.findByNameIgnoreCaseStartingWith(name);

        return cities.stream()
                .map(city -> weatherService.getWeather(city.getId()))
                .map(weatherInfo -> Map.of("temperature", weatherInfo.get("temperature"),
                        "name", weatherInfo.get("name")))
                .collect(Collectors.toList());
    }
    // END
}

