package exercise.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.List;
import java.util.Map;

import java.nio.file.Paths;
import java.nio.file.Files;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.commons.lang3.ArrayUtils;

public class UsersServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws IOException, ServletException {

        String pathInfo = request.getPathInfo();

        if (pathInfo == null) {
            showUsers(request, response);
            return;
        }

        String[] pathParts = pathInfo.split("/");
        String id = ArrayUtils.get(pathParts, 1, "");

        showUser(request, response, id);
    }

    private List getUsers() throws JsonProcessingException, IOException {
        // BEGIN
        String filepath = "build/resources/main/users.json";
        String content = Files.readString(Paths.get(filepath));
        ObjectMapper mapper = new ObjectMapper();
        List<Map<String, String>> users = mapper.readValue(content, List.class);
        return users;
        // END
    }

    private void showUsers(HttpServletRequest request,
                           HttpServletResponse response)
            throws IOException {

        // BEGIN
        List<Map<String, String>> users = getUsers();
        StringBuilder body = new StringBuilder();
        body.append("""
                <!DOCTYPE html>
                <html lang=\"ru\">
                    <head>
                        <meta charset=\"UTF-8\">
                        <title>Таблица со списком пользователей</title>
                        <link rel=\"stylesheet\" href=\"mysite.css\">
                        <style>
                            table, th, td {
                                border: 1px solid black;
                            }
                        </style>
                    </head>
                    <body>
                    <table>
                        <tr>
                """);

        for (Map<String, String> user : users) {
            body.append("<td>" + user.get("id") + "</td>");
            body.append("<td>");
            body.append("<a href=\"/users/" + user.get("id") + "\">" + user.get("firstName")
                    + " " + user.get("lastName") + "</a>");
            body.append("<td>");
        }

        body.append("""
                        </tr>
                    </table>
                    </body>
                </html>
                """);

        response.setContentType("text/html;charset=UTF-8");

        PrintWriter printWriter = response.getWriter();
        printWriter.write(body.toString());
        printWriter.close();
        // END
    }

    private void showUser(HttpServletRequest request,
                          HttpServletResponse response,
                          String id)
            throws IOException {

        // BEGIN
        List<Map<String, String>> users = getUsers();
        boolean isUserFound = users.stream()
                .anyMatch(user -> user.get("id").equals(id));

        if (isUserFound) {
            StringBuilder body = new StringBuilder();
            body.append("""
                    <!DOCTYPE html>
                    <html lang=\"ru\">
                        <head>
                            <meta charset=\"UTF-8\">
                            <title>Запрошенный пользователь</title>
                            <link rel=\"stylesheet\" href=\"mysite.css\">
                            <style>
                                table, th, td {
                                    border: 1px solid black;
                                }
                            </style>
                        </head>
                        <body>
                        <table>
                            <tr>
                    """);

            for (Map<String, String> user : users) {
                if (user.get("id").equals(id)) {
                    for (Map.Entry<String, String> entry : user.entrySet()) {
                        body.append("<td>" + entry.getKey() + ": " + entry.getValue() + "</td>");
                    }
                }
            }
            body.append("""
                            </tr>
                        </table>
                        </body>
                    </html>
                    """);

            response.setContentType("text/html;charset=UTF-8");

            PrintWriter printWriter = response.getWriter();
            printWriter.write(body.toString());
            printWriter.close();

        } else {
            response.sendError(404);
        }
        // END
    }
}
