package exercise;

import java.util.HashMap;
import java.util.Map;

// BEGIN
public class App {
    public static Map getWordCount(String sentence) {
        Map<String, Integer> dictionary = new HashMap<>();
        if (!"".equals(sentence)) {
            String[] sentenceToSplit = sentence.split(" ");
            int value = 1;
            for (String s : sentenceToSplit) {
                if (dictionary.containsKey(s)) {
                    int repeatValue = dictionary.get(s) + 1;
                    dictionary.put(s, repeatValue);
                } else {
                    dictionary.put(s, value);
                }
            }
        }
        return dictionary;
    }

    public static String toString(Map wordCount) {
        StringBuilder stringConcept = new StringBuilder();
        if (!wordCount.isEmpty()) {
            stringConcept.append("{\n");
            for (Object key : wordCount.keySet()) {
                stringConcept.append("  " + key + ": " + wordCount.get(key) + "\n");
            }
            stringConcept.append("}");
        } else {
            stringConcept.append("{}");
        }
        return stringConcept.toString();
    }
}
//END
