package exercise;

class App {
    // BEGIN
    public static String getTypeOfTriangle(int a, int b, int c) {
        if(a + b > c && b + c > a && c + a > b){
            if(a == b && b == c){
                String equilateral = "Равносторонний";
                System.out.println(equilateral);
                return equilateral;
            }
            else if(a == b && a != c || b == c && b !=a || a == c && a != b) {
                String isosceles = "Равнобедренный";
                System.out.println(isosceles);
                return isosceles;
            }
            else {
                String various = "Разносторонний";
                System.out.println(various);
                return various;
            }
        }
        else {
            String notFound = "Треугольник не существует";
            System.out.println(notFound);
            return notFound;
        }
    }
    // END
}
