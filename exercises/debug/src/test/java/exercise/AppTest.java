package exercise;

import org.junit.jupiter.api.Test;

import java.util.Scanner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.InstanceOfAssertFactories.atomicIntegerFieldUpdater;

class AppTest {
    @Test
    void testGetTypeOfTriangle1() {
        String result = App.getTypeOfTriangle(1, 2, 7);
        assertThat(result).isEqualTo("Треугольник не существует");
    }

    @Test
    void testGetTypeOfTriangle2() {
        String result = App.getTypeOfTriangle(5, 6, 7);
        assertThat(result).isEqualTo("Разносторонний");
    }

    @Test
    void testGetTypeOfTriangle3() {
        String result = App.getTypeOfTriangle(5, 6, 5);
        assertThat(result).isEqualTo("Равнобедренный");
    }

    @Test
    void testGetTypeOfTriangle4() {
        String result = App.getTypeOfTriangle(5, 5, 5);
        assertThat(result).isEqualTo("Равносторонний");
    }

    @Test
    void testGetTypeOfTriangle5() {
        String result = App.getTypeOfTriangle(1, -2, 7);
        assertThat(result).isEqualTo("Треугольник не существует");
    }

    // BEGIN
//    public static void main(String[] args) {
//        System.out.println(getFinalGrade(100, 12));
//        System.out.println(getFinalGrade(99, 0));
//        System.out.println(getFinalGrade(10, 15));
//        System.out.println(getFinalGrade(85, 5));
//        System.out.println(getFinalGrade(55, 3));
//        System.out.println(getFinalGrade(55, 0));
//    }
//
//    public static int getFinalGrade(int exam, int project) {
//
//        if (exam > 90 || project > 10) {
//            return 100;
//        } else if (exam > 75 && project >= 5) {
//            return 90;
//        } else if (exam > 50 && project >= 2) {
//            return 75;
//        } else {
//            return 0;
//        }
//    }
    // END
}
