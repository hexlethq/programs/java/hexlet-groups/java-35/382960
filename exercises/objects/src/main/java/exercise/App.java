package exercise;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Locale;

class App {
    // BEGIN
    public static String buildList(String[] items) {
        if (items.length == 0) {
            return "";
        }
        StringBuilder htmlList = new StringBuilder();
        for (String i : items) {
            htmlList.append("  <li>");
            htmlList.append(i);
            htmlList.append("</li>\n");
        }
        htmlList.insert(0, "<ul>\n");
        htmlList.append("</ul>");
        String strHtmlList = String.valueOf(htmlList);
        return strHtmlList;
    }

    public static String getUsersByYear(String[][] users, int year) {
        String[] usersByYear = new String[users.length];
        int count = 0;
        for (int i = 0; i < users.length; i++) {
            if (getDate(users[i][1]).getYear() == year) {
                usersByYear[count] = users[i][0];
                count++;

            }
        }
        usersByYear = Arrays.copyOf(usersByYear, count);
        String htmlUsersByYear = buildList(usersByYear);
        return htmlUsersByYear;
    }

    public static LocalDate getDate(String date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(date, formatter);
        return localDate;
    }

    public static LocalDate getDateDDMMMYYYY(String date) {
        DateTimeFormatter pattern = DateTimeFormatter.ofPattern("dd MMM yyyy");
        LocalDate dateToLocal = LocalDate.parse(date, pattern);
        return dateToLocal;
    }
// END

    // Это дополнительная задача, которая выполняется по желанию.
    public static String getYoungestUser(String[][] users, String date) throws Exception {
        // BEGIN
        LocalDate stringDateToLocal = getDateDDMMMYYYY(date);
        LocalDate dateAfter = stringDateToLocal;
        StringBuilder youngestUserBuilder = new StringBuilder();
        for (int i = 0; i < users.length; i++) {
            if (getDate(users[i][1]).isBefore(stringDateToLocal) && stringDateToLocal.isEqual(dateAfter) ||
                    getDate(users[i][1]).isBefore(stringDateToLocal) && getDate(users[i][1]).isAfter(dateAfter)) {
                dateAfter = getDate(users[i][1]);
                youngestUserBuilder.delete(0, youngestUserBuilder.length());
                youngestUserBuilder.append(users[i][0]);
            }
        }
        return String.valueOf(youngestUserBuilder);
    }
    // END
}