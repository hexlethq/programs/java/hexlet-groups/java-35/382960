package exercise;

import java.util.ArrayList;
import java.util.List;

// BEGIN
public class App {
    public static boolean scrabble(String letter, String word) {
        List<Character> listOfLetters = new ArrayList<>();
        List<Character> listOfWords = new ArrayList<>();
        getList(listOfLetters, letter);
        getList(listOfWords, word);
        int checker = 0;
        for (int j = 0; j < listOfWords.size(); j++) {
            if (listOfLetters.contains(listOfWords.get(j)) == true) {
                listOfLetters.remove(listOfWords.get(j));
                checker++;
                if (checker == word.length()) {
                    return true;
                }
            }
        }
        return false;
    }

    public static List getList(List list, String string) {
        for (int i = 0; i < string.length(); i++) {
            list.add(string.toLowerCase().charAt(i));
        }
        return list;
    }
}
//END
