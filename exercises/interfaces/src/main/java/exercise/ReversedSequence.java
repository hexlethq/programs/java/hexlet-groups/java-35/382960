package exercise;

// BEGIN
public class ReversedSequence implements CharSequence {
    private String text;

    public ReversedSequence(String setText) {
        this.text = new StringBuilder(setText).reverse().toString();
    }

    @Override
    public int length() {
        return this.text.length();
    }

    @Override
    public char charAt(int i) {
        return this.text.charAt(i - 1);
    }

    @Override
    public CharSequence subSequence(int i, int i1) {
        return this.text.subSequence(i, i1);
    }

    @Override
    public String toString() {
        return this.text;
    }
}
// END
