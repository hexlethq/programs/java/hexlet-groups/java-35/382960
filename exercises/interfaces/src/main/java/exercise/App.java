package exercise;

import java.util.List;

// BEGIN
public class App {
    public static List<String> buildAppartmentsList(List<Home> appartments, int count) {
        return appartments.stream()
                .sorted(Home::compareTo)
                .limit(count)
                .map(Home::toString)
                .toList();
    }
}
// END
