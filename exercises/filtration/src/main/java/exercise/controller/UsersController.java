package exercise.controller;
import com.querydsl.core.types.Predicate;
import exercise.model.User;
import exercise.model.QUser;
import exercise.repository.UserRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.web.bind.annotation.*;

// Зависимости для самостоятельной работы
// import org.springframework.data.querydsl.binding.QuerydslPredicate;
// import com.querydsl.core.types.Predicate;

@RestController
@RequestMapping("/users")
public class UsersController {

    @Autowired
    private UserRepository userRepository;

    // BEGIN
//    @GetMapping(path = "")
//    public Iterable<User> getUsersByFirstAndLastName(
//            @RequestParam(required = false) String firstName,
//            @RequestParam(required = false) String lastName) {
//        if (StringUtils.isBlank(firstName) && StringUtils.isBlank(lastName)) {
//            return userRepository.findAll();
//        }
//        if (StringUtils.isNotBlank(firstName) && StringUtils.isBlank(lastName)) {
//            return userRepository.findAll(QUser.user.firstName.containsIgnoreCase(firstName));
//        }
//        if (StringUtils.isBlank(firstName) && StringUtils.isNotBlank(lastName)) {
//            return userRepository.findAll(QUser.user.lastName.containsIgnoreCase(lastName));
//        }
//        return userRepository.findAll(
//                QUser.user.firstName.containsIgnoreCase(firstName)
//                        .and(
//                                QUser.user.lastName.containsIgnoreCase(lastName)
//                        )
//        );
//    }

    @GetMapping(path = "")
    public Iterable<User> getUsersByPredicate(@QuerydslPredicate(root = User.class) Predicate predicate) {
        return userRepository.findAll(predicate);
    }
    // END
}

