package exercise;

class App {
    public static void main(String[] args) {
        numbers();
        strings ();
        converting ();

    }
        public static void numbers () {
            // BEGIN
            System.out.println((8 / 2) + (100 % 3));
            // END
        }

        public static void strings () {
            String language = "Java";
            // BEGIN
            String work = " works";
            String machine = " on JVM";
            String allPhrase = language + work + machine;
            System.out.println(allPhrase);
            // END
        }

        public static void converting () {
            Number soldiersCount = 300;
            String name = "spartans";
            // BEGIN
            System.out.println(soldiersCount + " " + name);
            // END
        }
    }
