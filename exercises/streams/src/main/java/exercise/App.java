package exercise;

import java.util.List;

// BEGIN
public class App{
    public static long getCountOfFreeEmails(List<String> email){
        long amount = email.stream()
                .filter(freeEmail -> freeEmail.contains("@gmail.com")
                        || freeEmail.contains("@yandex.ru")
                        || freeEmail.contains("@hotmail.com"))
                .count();
        return amount;
    }
}

// END
