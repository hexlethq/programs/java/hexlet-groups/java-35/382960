package exercise;

import java.util.Arrays;

class Point {
    // BEGIN
    public static int[] makePoint(int x, int y) {
        int[] point = {x, y};
        return point;
    }

    public static int getX(int[] point) {
        int pointX = point[0];
        return pointX;
    }

    public static int getY(int[] point) {
        int pointY = point[1];
        return pointY;
    }

    public static String pointToString(int[] point) {
        String textPoint = "(" + point[0] + ", " + point[1] + ")";
        return textPoint;
    }

    public static int getQuadrant(int[] point) {
        int coordinateX = point[0];
        int coordinateY = point[1];
        if (coordinateX > 0 && coordinateY > 0) {
            return 1;
        } if (coordinateX < 0 && coordinateY > 0) {
            return 2;
        } if (coordinateX < 0 && coordinateY < 0) {
            return 3;
        } if (coordinateX > 0 && coordinateY < 0) {
            return 4;
        }
        return 0;
    }

    public static int[] getSymmetricalPointByX(int[] point) {
        int[] symmetricalPointByX = new int[2];
        symmetricalPointByX[0] = point[0];
        symmetricalPointByX[1] = -point[1];
        return symmetricalPointByX;
    }

    public static int calculateDistance(int[] point1, int[] point2) {
        int x1 = point1[0];
        int y1 = point1[1];
        int x2 = point2[0];
        int y2 = point2[1];
        int sideByX = Math.abs(x1 - x2);
        int sideByY = Math.abs(y1 - y2);
        int distance = (int) Math.hypot(sideByX, sideByY);
        return distance;
    }
    // END
}
