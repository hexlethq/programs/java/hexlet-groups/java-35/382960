package exercise;

class Triangle {
    // BEGIN
    public static double getSquare(double a, double b, double degree) {
        double square = Math.round(((a * b) / 2 * Math.sin(Math.toRadians(degree))) * 100);
        return square / 100;
    }

    public static void main(String[] args) {
        System.out.println(getSquare(4, 5, 45));
    }
    // END
}
