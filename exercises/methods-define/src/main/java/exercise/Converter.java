package exercise;

class Converter {
    // BEGIN
    public static int convert(int num, String measure) {
        if ("b".equals(measure)) {
            return num * 1024;
        } else if ("Kb".equals(measure)) {
            return num / 1024;
        } else
            return 0;
    }

    public static void main(String[] args) {
        int b = 10;
        int kb = convert(b, "b");
        String result = b + " Kb" + " = " + kb + " b";
        System.out.println(result);

    }
//    END
}
