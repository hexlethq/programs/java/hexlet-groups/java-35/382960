// BEGIN
package exercise;

import exercise.geometry.Point;
import exercise.geometry.Segment;

import java.util.Arrays;

class App {
    public static double[] getMidpointOfSegment(double[][] segment) {
        double x = (Point.getX(Segment.getBeginPoint(segment)) + Point.getX(Segment.getEndPoint(segment))) / 2;
        double y = (Point.getY(Segment.getBeginPoint(segment)) + Point.getY(Segment.getEndPoint(segment))) / 2;
        double[] midPoint = Point.makePoint(x, y);
//        double[] midPoint = new double[2];
//        for (int i = 0; i < midPoint.length; i++) {
//            midPoint[i] = (Segment.getBeginPoint(segment)[i] + Segment.getEndPoint(segment)[i]) / 2;
//        }
        return midPoint;
    }

    public static double[][] reverse(double[][] segment) {
        double[] beginPoint = Segment.getEndPoint(segment);
        double[] endPoint = Segment.getBeginPoint(segment);
        double[][] reverseSegment = {Arrays.copyOf(beginPoint, beginPoint.length), Arrays.copyOf(endPoint, endPoint.length)};
        return reverseSegment;
    }
    public static boolean isBelongToOneQuadrant(double[][] segment) {
        double[] beginPoint = Segment.getBeginPoint(segment);
        double[] endPoint = Segment.getEndPoint(segment);
        if (getQuadrant(beginPoint) == getQuadrant(endPoint) && getQuadrant(beginPoint) != 0 && getQuadrant(endPoint) != 0) {
            return true;
        }
        return false;
    }
    public static int getQuadrant(double[] point) {
        double coordinateX = point[0];
        double coordinateY = point[1];
        if (coordinateX > 0 && coordinateY > 0) {
            return 1;
        } if (coordinateX < 0 && coordinateY > 0) {
            return 2;
        } if (coordinateX < 0 && coordinateY < 0) {
            return 3;
        } if (coordinateX > 0 && coordinateY < 0) {
            return 4;
        }
        return 0;
    }
}
// END
