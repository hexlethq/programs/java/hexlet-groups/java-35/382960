// BEGIN
package exercise.geometry;

public class Segment {
    static double[][] segment;

    public static double[][] makeSegment(double[] point1, double[] point2) {
        segment = new double[][]{point1, point2};
        return segment;
    }

    public static double[] getBeginPoint(double[][] segment) {
        double[] beginPoint = segment[0];
        return beginPoint;
    }

    public static double[] getEndPoint(double[][] segment) {
        double[] endPoint = segment[1];
        return endPoint;
    }

}
// END
