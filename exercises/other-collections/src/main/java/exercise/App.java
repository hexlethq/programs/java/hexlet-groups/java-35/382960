package exercise;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

// BEGIN
public class App {
    public static LinkedHashMap<String, String> genDiff(Map<String, Object> data1, Map<String, Object> data2) {
        Set<String> keys = new TreeSet<>(data1.keySet());
        keys.addAll(data2.keySet());
        return keys.stream()
                .collect(Collectors.toMap(k -> k, v -> {
                    if (!data1.containsKey(v) && data2.containsKey(v)) {
                        return "added";
                    }
                    if (!data2.containsKey(v) && data1.containsKey(v)) {
                        return "deleted";
                    }
                    if (data1.containsKey(v) && data2.containsKey(v)) {
                        return data1.containsValue(data2.get(v)) ? "unchanged" : "changed";
                    }
                    return null;
                },(k1, k2) -> k1, LinkedHashMap::new));
    }
}
//END
