package exercise;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

// BEGIN
class App {
    public static void save(Path path, Car car) {
        try {
            Files.writeString(path, Car.serialize(car));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Car extract(Path path) {
        Car car = null;
        try {
            car = Car.unserialize(Files.readString(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return car;
    }
}
// END
