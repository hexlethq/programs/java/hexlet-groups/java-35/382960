package exercise;

import lombok.*;

// BEGIN
@Value
@AllArgsConstructor
@Getter
// END
class User {
    int id;
    String firstName;
    String lastName;
    int age;
}
