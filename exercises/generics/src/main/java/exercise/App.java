package exercise;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

// BEGIN
public class App {
    public static List<Map<String, String>> findWhere(List<Map<String, String>> book, Map<String, String> where) {
        List<Map<String, String>> result = new ArrayList<>();
        for (Map<String, String> iteratorForList : book) {
            int checker = 0;
            for (String iteratorForMap : where.keySet()) {
                if (iteratorForList.get(iteratorForMap).equals(where.get(iteratorForMap))) {
                    checker++;
                    if (checker == where.size()) {
                        result.add(iteratorForList);
                    }
                }
            }
        }
        return result;
    }
}
//END
