package exercise;

import java.util.Map;

// BEGIN
public class SingleTag extends Tag {

    public SingleTag(String setName, Map<String, String> setAttributes) {
        super(setName, setAttributes);
    }
}

// END
