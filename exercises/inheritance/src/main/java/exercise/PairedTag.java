package exercise;

import java.util.*;

// BEGIN
public class PairedTag extends Tag{
    private String bodyTag;
    private List<Tag> children;

    public PairedTag(String setName, Map<String, String> setAttributes, String setBody, List<Tag> setChildren) {
        super(setName, setAttributes);
        this.bodyTag = setBody;
        this.children = new ArrayList<>(setChildren);
    }

    private String childrenToString(List<Tag> children) {
        StringBuilder sChildrenList = new StringBuilder();
        for (Tag singleTag : children) {
            sChildrenList.append(singleTag.toString());
        }
        return sChildrenList.toString().trim();
    }

    @Override
    public String toString() {
        String sReturn = super.toString() + childrenToString(this.children) + this.bodyTag + "</" + super.nameTag + ">";
        return sReturn.trim();
    }
}
// END
