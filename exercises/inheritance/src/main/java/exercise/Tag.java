package exercise;

import java.util.LinkedHashMap;
import java.util.Map;

// BEGIN
public class Tag {
    public String nameTag;
    public Map <String, String> attributesTag;

    public Tag(String setName, Map<String, String> setAttributes) {
        this.nameTag = setName;
        this.attributesTag = new LinkedHashMap<>(setAttributes);
    }

    public String attributesToString(Map<String, String> map) {
        StringBuilder sAttributes = new StringBuilder();
        for (Map.Entry<String, String> entry : map.entrySet()) {
            sAttributes.append(entry.getKey())
                    .append("=")
                    .append("\"")
                    .append(entry.getValue())
                    .append("\" ");
        }
        return sAttributes.toString().trim();
    }

    @Override
    public String toString() {
        String sToString = nameTag + " " + attributesToString(attributesTag);
        return "<" + sToString.trim()+ ">";
    }
}
// END
