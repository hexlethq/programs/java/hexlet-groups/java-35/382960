package exercise.servlet;

import exercise.Data;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.function.Predicate;

public class CompaniesServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
                throws IOException, ServletException {

        // BEGIN
        PrintWriter printWriter = response.getWriter();
        boolean isNotEmptySearchRequest = request.getQueryString() != null
                && request.getQueryString().contains("search")
                && !request.getParameter("search").equals("");
        if (isNotEmptySearchRequest) {
            boolean isFound = Data.getCompanies().stream()
                    .anyMatch(company -> company.toString().contains(request.getParameter("search")));
            if (isFound) {
                Data.getCompanies().stream()
                        .filter(company -> company.toString().contains(request.getParameter("search")))
                        .forEach(company -> printWriter.println(company));
            } else {
                printWriter.println("Companies not found");
            }
        } else {
            Data.getCompanies().stream()
                    .forEach(printWriter::println);
        }
        // END
    }
}
