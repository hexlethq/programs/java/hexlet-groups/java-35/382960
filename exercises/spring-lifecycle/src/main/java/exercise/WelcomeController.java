package exercise;

import exercise.daytimes.Daytime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

// BEGIN
@RestController
public class WelcomeController {
    @Autowired
    private Meal meal;

    @Autowired
    private ApplicationContext context;

    @GetMapping(path = "/daytime")
    public String welcome() {
        String currentDayTime = context.getBean("dayTime", Daytime.class).getName();
        String result = "It is " + currentDayTime + " now. Enjoy your "
                + meal.getMealForDaytime(currentDayTime);
        return result;
    }
}
// END
