package exercise;

import java.util.Arrays;
import java.util.stream.Collectors;

// BEGIN
public class App {
    public static String getForwardedVariables(String fileName) {
        String result = fileName.lines()
                .filter(x -> x.startsWith("environment="))
                .map(a -> a.replaceAll("environment=", ""))
                .map(a -> a.replaceAll("\"", ""))
                .filter(a -> a.contains("X_FORWARDED_"))
                .map(a -> a.split(","))
                .flatMap(Arrays::stream)
                .filter(a -> a.startsWith("X_FORWARDED_"))
                .map(a -> a.replaceAll("X_FORWARDED_", ""))
                .collect(Collectors.joining(","));
        return result;
    }
}
//END
