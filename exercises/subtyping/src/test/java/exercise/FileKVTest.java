package exercise;

import java.util.HashMap;
import org.junit.jupiter.api.BeforeEach;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
// BEGIN
import static org.assertj.core.api.Assertions.assertThat;
// END


class FileKVTest {

    private static Path filepath = Paths.get("src/test/resources/file").toAbsolutePath().normalize();

    @BeforeEach
    public void beforeEach() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        String content = mapper.writeValueAsString(new HashMap<String, String>());
        Files.writeString(filepath, content, StandardOpenOption.CREATE);
    }

    // BEGIN
    @Test
    void fileKVTest() {
        KeyValueStorage storage = new FileKV("src/test/resources/file", Map.of("key", "value"));

        assertThat(storage.get("key", "")).isEqualTo("value");

        storage.set("manUTD", "best club");
        storage.set("chelsea", "is not");

        assertThat(storage.get("manUTD", "")).isEqualTo("best club");
        assertThat(storage.get("chelsea", "")).isEqualTo("is not");

        storage.unset("chelsea");
        storage.unset("manUTD");

        assertThat(storage.get("chelsea", "default")).isEqualTo("default");
        assertThat(storage.get("manUTD", "default")).isEqualTo("default");
        assertThat(storage.toMap()).isEqualTo(Map.of("key", "value"));

    }
    // END
}
