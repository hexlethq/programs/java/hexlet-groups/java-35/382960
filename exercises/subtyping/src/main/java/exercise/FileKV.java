package exercise;

import java.util.HashMap;
import java.util.Map;

// BEGIN
class FileKV implements KeyValueStorage{
    private final String filepath;

    public FileKV(String filepath, Map<String, String> data) {
        this.filepath = filepath;
        Map<String, String> storage = new HashMap<>(data);
        String content = Utils.serialize(storage);
        Utils.writeFile(filepath, content);
    }

    @Override
    public void set(String key, String value) {
        String readContent = Utils.readFile(filepath);
        Map<String, String> storage = Utils.unserialize(readContent);
        storage.put(key, value);
        String writeContent = Utils.serialize(storage);
        Utils.writeFile(filepath, writeContent);
    }

    @Override
    public void unset(String key) {
        String readContent = Utils.readFile(filepath);
        Map<String, String> storage = Utils.unserialize(readContent);
        storage.remove(key);
        String writeContent = Utils.serialize(storage);
        Utils.writeFile(filepath, writeContent);
    }

    @Override
    public String get(String key, String defaultValue) {
        String readContent = Utils.readFile(filepath);
        Map<String, String> storage = Utils.unserialize(readContent);
        if (storage.containsKey(key)) {
            return storage.get(key);
        }
        return defaultValue;
    }

    @Override
    public Map<String, String> toMap() {
        String readContent = Utils.readFile(filepath);
        return Utils.unserialize(readContent);
    }
}
// END
