package exercise;

import java.lang.reflect.Field;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

// BEGIN
class Validator {

    public static List<String> validate(Address address) {
        List<String> notValidated = new LinkedList<>();

        for (Field field : address.getClass().getDeclaredFields()) {
            NotNull notNull = field.getAnnotation(NotNull.class);
            field.setAccessible(true);
            try {
                if (notNull != null && field.get(address) == null) {
                    notValidated.add(field.getName());
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return notValidated;
    }

    public static Map<String, List<String>> advancedValidate(Address address) {
        Map<String, List<String>> notValidated = new LinkedHashMap<>();

        for (Field field : address.getClass().getDeclaredFields()) {

            List<String> errorDescription = new LinkedList<>();

            NotNull notNull = field.getAnnotation(NotNull.class);
            MinLength minLengthAnnotation = field.getAnnotation(MinLength.class);

            field.setAccessible(true);
            try {
                if (notNull != null && field.get(address) == null) {
                    errorDescription.add("can not be null");
                }
                if (minLengthAnnotation != null && (field.get(address) == null
                        || field.get(address).toString().length() < minLengthAnnotation.minLength())) {
                    errorDescription.add("length less than " + minLengthAnnotation.minLength());
                }
                if (!errorDescription.isEmpty()) {
                    notValidated.put(field.getName(), errorDescription);
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return notValidated;
    }
}
// END
