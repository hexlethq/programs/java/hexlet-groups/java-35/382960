package exercise;

import java.util.Arrays;

class App {
    // BEGIN
    public static int getIndexOfMaxNegative(int[] numbers) {
        int maxNegativeIndex = -1;
        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] < 0 && maxNegativeIndex == -1 || numbers[i] < 0 && numbers[i] > numbers[maxNegativeIndex]) {
                maxNegativeIndex = i;
            }
        }
        return maxNegativeIndex;
    }

    public static int[] getElementsLessAverage(int[] numbers) {
        if (numbers.length == 0) {
            return numbers;
        }
        int sum = sumOfElements(numbers);
        int amountOfElements = numbers.length;
        int averageSum = sum / amountOfElements;
        int newLength = 0;
        for (int b : numbers) {
            newLength = b <= averageSum ? (newLength + 1) : newLength;
        }
        int[] result = new int[newLength];
        int indexResult = 0;
        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] <= averageSum) {
                result[indexResult] = numbers[i];
                indexResult += 1;
            }
        }
        return result;
    }

    public static int getSumBeforeMinAndMax(int[] numbers) {
        if (numbers.length == 0) {
            return -1;
        }
        int indexOfMax = -1;
        int indexOfMin = -1;
        for (int i = 0; i < numbers.length; i++) {
            if (indexOfMax == -1 || numbers[i] > numbers[indexOfMax]) {
                indexOfMax = i;
            }
            if (indexOfMin == -1 || numbers[i] < numbers[indexOfMin]) {
                indexOfMin = i;
            }
        }
        int sumBeforeMinAndMax = 0;
        for (int j = 0; j < numbers.length; j++) {
            if (j < indexOfMax && j > indexOfMin || j > indexOfMax && j < indexOfMin) {
                sumBeforeMinAndMax += numbers[j];
            }
        }
        return sumBeforeMinAndMax;
    }

    public static int sumOfElements(int[] array) {
        int sum = 0;
        for (int i : array) {
            sum += i;
        }
        return sum;
    }
}

// END
//{-30, 42, -5, 31, -37, 25, -50}