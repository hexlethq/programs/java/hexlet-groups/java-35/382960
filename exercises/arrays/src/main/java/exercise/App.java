package exercise;

class App {
    // BEGIN
    public static int[] reverse(int[] numbers) {
        int[] newNumbers = new int[numbers.length];
        if (numbers.length != 0) {
            for (int i = 0; i <= numbers.length - 1; i++) {
                newNumbers[i] = numbers[(numbers.length - 1) - i];
            }
            return newNumbers;
        }
        return numbers;
    }

    public static int mult(int[] secNumbers) {
        int multiplication = 1;
        for (int i : secNumbers) {
            multiplication = multiplication * i;
        }
        return multiplication;
    }

    public static int[] flattenMatrix(int[][] matrix) {
        if (matrix.length == 0) {
            int[] emptyArray = new int[0];
            return emptyArray;
        }
        int arrayLength = 0;
        int indexArray = 0;
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                arrayLength += 1;
            }
        }
        int[] array = new int[arrayLength];
        for (int x = 0; x < matrix.length; x++) {
            for (int y = 0; y < matrix[x].length; y++) {
                array[indexArray] = matrix[x][y];
                indexArray += 1;
            }
        }
        return array;
    }
}
// END