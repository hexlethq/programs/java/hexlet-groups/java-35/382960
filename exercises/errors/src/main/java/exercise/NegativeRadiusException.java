package exercise;

// BEGIN
class NegativeRadiusException extends Exception{
    public NegativeRadiusException(String setMessage) {
        super(setMessage);
    }
}
// END
