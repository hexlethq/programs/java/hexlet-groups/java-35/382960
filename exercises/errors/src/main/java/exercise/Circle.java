package exercise;

// BEGIN
class Circle {
    private final int radius;

    public Circle(Point setPoint, int setRadius) {
        Point centrePoint = setPoint;
        this.radius = setRadius;
    }

    public int getRadius() {
        return this.radius;
    }

    public double getSquare() throws NegativeRadiusException {
        if (this.radius < 0) {
            throw new NegativeRadiusException("Не удалось посчитать площадь");
        }
        return Math.PI * Math.pow(radius, 2);
    }
}
// END
