package exercise;


import java.util.Arrays;

// BEGIN
public class Kennel {
    private static String[][] puppiesInKennel = new String[0][2];

    public static void addPuppy(String[] puppy) {
        String[][] newPuppiesInKennel = new String[puppiesInKennel.length + 1][2];
        int count = 0;
        for (int i = 0; i < newPuppiesInKennel[count].length; i++) {
            newPuppiesInKennel[count][i] = puppy[i];
        }
        count += 1;
        puppiesInKennel = Arrays.copyOf(newPuppiesInKennel, newPuppiesInKennel.length);
    }

    public static void addSomePuppies(String[][] puppies) {
        String[][] newPuppiesInKennel = Arrays.copyOf(puppiesInKennel, puppies.length + puppiesInKennel.length);
        int indexPuppies = 0;
        for (int i = puppiesInKennel.length; i < newPuppiesInKennel.length; i++) {
            newPuppiesInKennel[i] = puppies[indexPuppies];
            indexPuppies++;
        }
        puppiesInKennel = Arrays.copyOf(newPuppiesInKennel, newPuppiesInKennel.length);
    }

    public static int getPuppyCount() {
        int count = puppiesInKennel.length;
        return count;
    }

    public static Boolean isContainPuppy(String name) {
        for (int i = 0; i < puppiesInKennel.length; i++) {
            if (name.equals(puppiesInKennel[i][0])) {
                return true;
            }
        }
        return false;
    }

    public static String[][] getAllPuppies() {
        return puppiesInKennel;
    }

    public static String[] getNamesByBreed(String breed) {
        String[] namesByBreed = new String[getNewLengthByExpect(puppiesInKennel, breed)];
        int count = 0;
        for (int i = 0; i < puppiesInKennel.length; i++) {
            if (breed.equals(puppiesInKennel[i][1])) {
                namesByBreed[count] = puppiesInKennel[i][0];
                count += 1;
            }
        }
        return namesByBreed;
    }

    public static int getNewLengthByExpect(String[][] array, String expect) {
        int length = 0;
        for (int i = 0; i < array.length; i++) {
            if (expect.equals(array[i][1])) {
                length++;
            }
        }
        return length;
    }

    public static void resetKennel() {
        puppiesInKennel = new String[0][0];
    }

    public static Boolean removePuppy(String name) {
        if (isContainPuppy(name)) {
            String[][] newPuppies = new String[puppiesInKennel.length - 1][2];
            int count = 0;
            for (int i = 0; i < puppiesInKennel.length; i++) {
                if (!name.equals(puppiesInKennel[i][0])) {
                    newPuppies[count] = puppiesInKennel[i];
                    count++;
                }
            }
            puppiesInKennel = Arrays.copyOf(newPuppies, newPuppies.length);
            return true;
        }
        return false;
    }
}

// END
