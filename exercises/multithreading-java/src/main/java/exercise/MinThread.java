package exercise;

// BEGIN
public class MinThread extends Thread {
    private int[] array;
    private int min;

    public MinThread(int[] array) {
        this.array = array;
    }

    public int getMin() {
        return min;
    }

    @Override
    public void run() {
        this.min = this.array[0];
        for (int i = 1; i < this.array.length; i++) {
            if (this.min > this.array[i]) {
                this.min = this.array[i];
            }
        }
    }
}
// END
