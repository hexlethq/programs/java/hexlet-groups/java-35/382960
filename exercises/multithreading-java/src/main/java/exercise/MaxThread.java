package exercise;

// BEGIN
public class MaxThread extends Thread {
    private int[] array;
    private int max;

    public MaxThread(int[] array) {
        this.array = array;
    }

    @Override
    public void run() {
        this.max = this.array[0];
        for (int i = 1; i < this.array.length; i++) {
            if (this.max < this.array[i]) {
                this.max = this.array[i];
            }
        }
    }

    public int getMax() {
        return max;
    }
}
// END
