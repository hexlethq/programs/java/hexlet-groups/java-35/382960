package exercise;

import java.util.Map;
import java.util.logging.Logger;
import java.util.logging.Level;




class App {
    private static final Logger LOGGER = Logger.getLogger("AppLogger");

    // BEGIN
    public static Map<String, Integer> getMinMax(int[] array) {
        LOGGER.info("MINTHREAD STARTED");
        LOGGER.info("MAXTHREAD STARTED");
        MinThread minThread = new MinThread(array);
        MaxThread maxThread = new MaxThread(array);
        minThread.start();
        maxThread.start();
        try {
            minThread.join();
            LOGGER.info("MINTHREAD FINISHED");
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        try {
            maxThread.join();
            LOGGER.info("MAXTHREAD FINISHED");
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        LOGGER.info("Result is: " + minThread.getMin() + " " + maxThread.getMax());
        return Map.of("min", minThread.getMin(), "max", maxThread.getMax());
    }
    // END
}

