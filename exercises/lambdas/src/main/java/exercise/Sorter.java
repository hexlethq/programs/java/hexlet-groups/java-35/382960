package exercise;

import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.Map;
import java.util.List;
import java.time.LocalDate;

// BEGIN
public class Sorter {
    public static List<String> takeOldestMan(List<Map<String, String>> users) {
        List<String> sorted = users.stream()
                .filter(male -> male.get("gender").equals("male"))
                .sorted(Comparator.comparing(male -> getYear(male.get("birthday"))))
                .map(x -> x.get("name"))
                .toList();
        return sorted;
    }

    private static LocalDate getYear(String birthday) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(birthday, formatter);
        return localDate;
    }
}
// END
