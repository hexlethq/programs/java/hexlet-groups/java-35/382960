package exercise;

import java.util.Arrays;
import java.util.function.Function;
import java.util.stream.Stream;

// BEGIN
public class App {
    public static String[][] enlargeArrayImage(String[][] image) {
        String[][] image2 = Arrays.stream(image)
                .flatMap(a -> Stream.of(a, a))
                .map(doubleValueOfArray)
                .toArray(String[][]::new);
        return image2;
    }

static Function<String[], String[]> doubleValueOfArray = strings -> Arrays.stream(strings)
        .flatMap(value -> Stream.of(value, value))
        .toArray(String[]::new);
}
// END
