package exercise;

import java.util.Arrays;

class SafetyList {
    // BEGIN
    private int length = 10;
    private int[] array = new int[length];
    private int size = 0;

    public synchronized void add(int x) {
        if (size >= length - 1) {
            length = length * 2;
            this.array = Arrays.copyOf(this.array, length);
        }
        this.array[size] = x;
        size++;
    }

    public int get(int index) {
        return this.array[index];
    }

    public int getSize() {
        return this.size;
    }
    // END
}
