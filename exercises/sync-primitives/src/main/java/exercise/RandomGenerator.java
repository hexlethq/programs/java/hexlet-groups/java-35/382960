package exercise;

import java.util.Random;

public class RandomGenerator {
    private int min;
    private int max;

    public RandomGenerator(int min, int max) {
        this.min = min;
        this.max = max;
    }

    public int getRandomNumber() {
        int diff = max - min;
        Random random = new Random();
        int i = random.nextInt(diff + 1);
        return i += min;
    }
}
